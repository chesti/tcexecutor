﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TelentCommandExecutor;

namespace TestConsoleApp
{
    class ProgramObject
    {
        private TCExecutor mSrv;
        List<TestObject> testObjects;

        public ProgramObject()
        {
            testObjects = new List<TestObject>();

            mSrv = new TCExecutor();

            CmdExecutor exec;

            exec = mSrv.AddCommand("add");
            exec.ShortHelpString = "Adds new test object";
            exec.HelpString = "Adds new test object. For more detailed information check subcommands.";

            exec = mSrv.AddCommand("add object");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(addObject);
            exec.ShortHelpString = "Adds new test object";
            exec.HelpString = "add object name=<mane> msg=<message>";

            exec = mSrv.AddCommand("remove");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(removeObject);
            exec.ShortHelpString = "Removes object from a list";
            exec.HelpString = "remove id=<n>";

            exec = mSrv.AddCommand("list");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(listObjects);
            exec.ShortHelpString = "Lists all test objects";
            exec.HelpString = "list";

            exec = mSrv.AddCommand("display");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(dispalyServerMessageBox);
            exec.ShortHelpString = "Displays object message box on server!!!";
            exec.HelpString = "display id=<n>";
        }

        string dispalyServerMessageBox(Cmd command)
        {
            if (command.CmdParams.ContainsKey("id"))
            {
                try
                {
                    int id = Convert.ToInt32(command.CmdParams["id"]);
                    if (testObjects.Count > id)
                    {
                        MessageBox.Show(testObjects[id].ToString(), "Server message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return "Message box shown on a server.";
                    }
                    else
                    {
                        return "Index is badly defined";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            return "Missing id attribute";
        }

        string removeObject(Cmd command)
        {
            if (command.CmdParams.ContainsKey("id"))
            {
                try
                {
                    int id = Convert.ToInt32(command.CmdParams["id"]);
                    if (testObjects.Count > id)
                    {
                        testObjects.RemoveAt(id);
                        return "Object removed. Try list to check";
                    }
                    else
                    {
                        return "Index is badly defined";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            return "Missing id attribute";
        }

        string listObjects(Cmd command)
        {
            string result = "User defined objects:\n\r";
            int i = 0;
            foreach (TestObject obj in testObjects)
            {
                result += String.Format("{0}. {1}\n\r", i, obj.ToString());
                i++;
            }
            return result;
        }

        public void Start()
        {
            System.Net.IPEndPoint endPoint = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 9999);
            mSrv.StartServer(endPoint);

            Console.ReadKey();

            mSrv.StopServer();

        }

        private string addObject(Cmd command)
        {
            if (command.CmdParams.ContainsKey("name") &&
                command.CmdParams.ContainsKey("msg"))
            {
                string name = (string)command.CmdParams["name"];
                string msg = (string)command.CmdParams["msg"];
                TestObject obj = new TestObject(name, msg);
                testObjects.Add(obj);
                return "Object created: " + obj.ToString();
            }
            return "Badly defined parameters";
        }


    }
}
