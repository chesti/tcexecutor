﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsoleApp
{
    class TestObject
    {
        public string Name { get; private set;}
        public string Message {get; private set;}

        public TestObject(string name, string message)
        {
            Name = name;
            Message = message;
        }

        public override string ToString()
        {
            return String.Format("Name: {0}, Message: {1}", Name, Message);
        }
    }
}
