using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using TelentCommandExecutor.Net;

namespace TelentCommandExecutor
{
    /// <summary>
    /// 
    /// </summary>
    public class TCExecutor
    {
        #region private/internal

        /// <summary>
        /// Commands defined for the executor
        /// </summary>
        private CmdExecutor mCommands;

        /// <summary>
        /// Simple TCP listener
        /// </summary>
        private Listener mConnectionsListener;

        /// <summary>
        /// Welcome string. Shown after user logon.
        /// </summary>
        internal string WelcomeString { get; private set; }

        #endregion

        #region constructor

        /// <summary>
        /// TCExecutor is a main project object. Make an instance of it to add a new functionality to your app.
        /// </summary>
        public TCExecutor()
        {
            mCommands = new CmdExecutor(">", null);
            mCommands.ExecutorEvent += new CmdExecutor.ExecutorHandler(commandUnknown);
            mCommands.HelpString = "Telnet Command Parser";

            CmdExecutor exec = AddCommand("?");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(HelpString);
            exec.ShortHelpString = "Use to dispaly Help";

            exec = AddCommand("exit");
            exec.ExecutorEvent += new CmdExecutor.ExecutorHandler(ExitCmd);
            exec.HelpString = "Closes your connection";
            exec.ShortHelpString = "Close the conection.";

        }

        #endregion

        #region public methods
        /// <summary>
        /// Starts server. Stratrs to listen to incoming connections on port. 
        /// This constructor starts separate threads for functionality.
        /// </summary>
        /// <param name="endPoint">System.Net.IPEndPoint defines form where connection is accepted
        /// Example: new System.Net.IPEndPoint(System.Net.IPAddress.Any, 9999); will start listening to any incoming connection on port 9999</param>
        /// <param name="welcomeString">Welcome string shown after user logon.</param>
        public void StartServer(System.Net.IPEndPoint endPoint, string welcomeString = "Welcome to Telnet Command Parser. Type ? to get commands list.")
        {
            WelcomeString = welcomeString;
            //starting the server
            mConnectionsListener = new Listener(endPoint, this);
            //this method works asynchonously. There is no need to start it in a separate thread.
            mConnectionsListener.Run();
        }

        /// <summary>
        /// Stops Telent Command Executor thread.
        /// </summary>
        public void StopServer()
        {
            mConnectionsListener.StopListener();
        }

        /// <summary>
        /// Use for configuration. Define supported commands
        /// </summary>
        /// <param name="commandName">commands. Example: set name</param>
        /// <returns>Executor from which Event can be used.</returns>
        public CmdExecutor AddCommand(string commandName)
        {
            Cmd c = new Cmd(commandName);

            return mCommands.AddCommand(c.CommandQueue);
        }
        #endregion

        #region private/internal methods
        /// <summary>
        /// Search for Executor and invokes delegates.
        /// </summary>
        /// <param name="commandToParse">command string which should be invoked.</param>
        internal string ExecuteCommand(string commandToParse)
        {
            try
            {
                Cmd c = new Cmd(commandToParse);
                CmdExecutor exec = mCommands.FindExecutor(c.CommandQueue);

                if (exec == null)
                {
                    //command not found.
                    c.SetARGVParameters(mCommands.GetFullCommandName());
                    return mCommands.OnComandHit(c);
                }
                else
                {
                    c.SetARGVParameters(exec.GetFullCommandName());
                    return exec.OnComandHit(c);
                }
            }
            catch (CmdParseException ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Delegate which supports unknown command.
        /// </summary>
        /// <param name="command">unknown command object.</param>
        private string commandUnknown(Cmd command)
        {
            return String.Format("Unknown command: {0} \r\n Try command ? to display help.", command.OrigCMD);
        }


        /// <summary>
        /// Supports exit command.
        /// </summary>
        /// <param name="command">Exit command object</param>
        /// <returns>Exit string</returns>
        private string ExitCmd(Cmd command)
        {
            return "exit";
        }


        /// <summary>
        /// Returns help string defnined during TCExecutor configuration
        /// </summary>
        /// <param name="command">Command object</param>
        /// <returns>Help string of input command</returns>
        private string HelpString(Cmd command)
        {
            //remove ? from start.
            Queue<string> c = command.CommandQueue;
            string param = c.Dequeue();

            CmdExecutor exec = mCommands;
            if (c.Count > 0)
                exec = exec.FindExecutor(c);

            if (exec != null)
                return String.Format("==========\r\n{0}", exec.GetHelpString());
            else
                return "Unable to display help. Command unknown.";


        }
        #endregion

    }
}
