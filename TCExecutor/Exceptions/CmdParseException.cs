﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelentCommandExecutor
{
    /// <summary>
    /// Exception thrown when command parsing error occurs
    /// </summary>
    public class CmdParseException : ArgumentException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        public CmdParseException(string msg)
            : base(msg)
        {
        }
    }
}
