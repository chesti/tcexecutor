using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TelentCommandExecutor
{
    /// <summary>
    /// 
    /// </summary>
    public class Cmd
    {
        /// <summary>
        /// This is a parsing reg exp. It matches "cmd1 cmd2 arg1=v1 arg2=v2"
        /// </summary>
        private static Regex mCmdRegExp = new Regex("(\"[^\"]+\")|([^ \"=]+)|(=)", RegexOptions.Compiled);

        #region properties
        private string mOrigCMD;
        /// <summary>
        /// 
        /// </summary>
        public string OrigCMD
        {
            get { return mOrigCMD; }
        }
        /// <summary>
        /// This holds command elements. Example ["cmd1", "cmd2"]
        /// </summary>
        private Stack<string> mCmd;

        internal Queue<string> CommandQueue
        {
            get { return new Queue<string>(mCmd.Reverse().AsEnumerable()); }
        }

        /// <summary>
        /// This holds command parameters. Example ["arg1"=>"v1, "arg2"=>"v2"]
        /// </summary>
        private Dictionary<string, object> mCmdParams;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, object> CmdParams
        {
            get { return mCmdParams; }
        }

        #endregion

        #region constructors
        /// <summary>
        /// Class Cmd constructor. Splits commandToParse into elements.
        /// </summary>
        /// <param name="commandToParse">Command line whcih should be parsed</param>
        public Cmd(string commandToParse)
        {
            if (commandToParse == null) return;
            mOrigCMD = commandToParse;

            mCmd = new Stack<string>();
            mCmdParams = new Dictionary<string, object>();

            Match m = mCmdRegExp.Match(commandToParse);

            bool parameter = false;

            while (m.Success)
            {
                string v = m.Value.Replace("\"", "");
                switch (v)
                {
                    case "=":
                        //if previously parameter was parsed than we have sth like cmd x=y=z. Forbiden.
                        if (parameter) throw new CmdParseException("No support for x=y=z. Too many = in command.");

                        m = m.NextMatch();
                        if (m.Success)
                            mCmdParams.Add(mCmd.Pop(), m.Value);

                        parameter = true;
                        break;
                    default:
                        mCmd.Push(v);
                        parameter = false;
                        break;
                }
                m = m.NextMatch();
            }
        }
        #endregion

        #region methods

        /// <summary>
        /// If parameters doesn't have key then they are parsed as ARGV params.
        /// Example: 
        /// command: <b>set</b> a=10 b=12 x y z 
        /// CmdParams = {"a" => "10", "b" => "12", "ARGV" => List ["a","b","c"]
        /// </summary>
        /// <param name="commandExecuted">Command from end user.</param>
        internal void SetARGVParameters(string commandExecuted)
        {
            string[] cmdElem = commandExecuted.Split(' ');
            List<string> argvParams = new List<string>(mCmd.AsEnumerable());

            foreach (string c in cmdElem)
            {
                argvParams.Remove(c);
            }

            mCmdParams.Add("ARGV", argvParams);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return mOrigCMD;
        }

        #endregion
    }
}
