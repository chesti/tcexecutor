using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelentCommandExecutor
{
    /// <summary>
    /// 
    /// </summary>
    public class CmdExecutor
    {
        #region public

        /// <summary>
        /// Delegate for command invocation
        /// </summary>
        /// <param name="command">Command object. Parsed telent input command.</param>
        /// <returns>String which should be send to end user.</returns>
        public delegate string ExecutorHandler(Cmd command);

        /// <summary>
        /// Event hit when command arrives.
        /// </summary>
        public event ExecutorHandler ExecutorEvent;

        /// <summary>
        /// Get/Set of command help string.
        /// </summary>
        public string HelpString
        {
            get
            {
                return mHelpString;
            }
            set
            {
                mHelpString = value;
            }
        }

        /// <summary>
        /// Get/Set of command short string. 
        /// </summary>
        public string ShortHelpString
        {
            get { return mShortHelpString; }
            set { mShortHelpString = value; }
        }

        #endregion

        #region private/internal
        /// <summary>
        /// subcommands
        /// </summary>
        internal List<CmdExecutor> mChildren { get; private set; }
        internal string mCmdName { get; private set; }
        internal CmdExecutor mParent { get; private set; }
        internal string mHelpString { get; set; }
        internal string mShortHelpString { get; set; }
        #endregion

        #region private/internal methods

        /// <summary>
        /// CmdExecutor constructor. Can be invoked just in DLL. Returned after TCExecitor.AddCommand(...)
        /// </summary>
        /// <param name="cmdName">String of command name</param>
        /// <param name="parent">Parent command.</param>
        internal CmdExecutor(string cmdName, CmdExecutor parent)
        {
            mCmdName = cmdName;
            mParent = parent;
            mHelpString = "";
            mChildren = new List<CmdExecutor>();
        }

        /// <summary>
        /// Returns parsed help string.
        /// </summary>
        /// <returns>Parsed help message.</returns>
        internal string GetHelpString()
        {
            string result = string.Format("COMMAND: {0}\r\n{1}", GetFullCommandName(), mHelpString);

            if (mChildren.Capacity > 0) result += "\r\nSubcommands:\r\n";

            foreach (CmdExecutor executor in mChildren)
            {
                result += string.Format("   {0}", executor.mCmdName);
                if (executor.mShortHelpString != "")
                    result += string.Format(" = {0}", executor.mShortHelpString);

                result += "\r\n";
            }

            return result.TrimEnd();
        }

        /// <summary>
        /// Returns full command name. Including parent command.
        /// </summary>
        /// <returns>Full command name</returns>
        internal string GetFullCommandName()
        {
            if (mParent != null)
                return mParent.GetFullCommandName() + " " + mCmdName;

            return mCmdName;
        }

        /// <summary>
        /// Adds command to the tree and returns last command executor.
        /// </summary>
        /// <param name="cmd">Cmd.CommandQueue</param>
        /// <returns>CmdExecutor of last command</returns>
        internal CmdExecutor AddCommand(Queue<string> cmd)
        {
            CmdExecutor result = null;

            if (cmd.Count == 0)
                return result;


            string paramName = cmd.Dequeue();

            foreach (CmdExecutor executor in mChildren)
            {
                if (executor.mCmdName.Equals(paramName))
                {
                    result = executor.AddCommand(cmd);
                    if (result == null)
                        return executor;

                    return result;
                }
            }

            //element not found. Create it.
            CmdExecutor newExecutor = new CmdExecutor(paramName, this);
            mChildren.Add(newExecutor);

            result = newExecutor.AddCommand(cmd);
            if (result == null)
                return newExecutor;

            return result;
        }

        /// <summary>
        /// Recursive finding Executor in tree.
        /// </summary>
        /// <param name="cmd">Cmd.CommandQueue</param>
        /// <returns>Command Executor</returns>
        internal CmdExecutor FindExecutor(Queue<string> cmd)
        {
            CmdExecutor result = null;

            if (cmd.Count == 0)
                return result;


            string paramName = cmd.Dequeue();

            foreach (CmdExecutor executor in mChildren)
            {
                if (executor.mCmdName.Equals(paramName))
                {
                    result = executor.FindExecutor(cmd);
                    if (result == null)
                        return executor;

                    return result;
                }
            }

            return result;
        }

        /// <summary>
        /// Event implementation to hit all delegates
        /// </summary>
        /// <param name="command">Cmd passed to delegates in "oudside world"</param>
        internal string OnComandHit(Cmd command)
        {
            ExecutorHandler handler = ExecutorEvent;
            if (handler != null)
            {
                return handler(command);
            }
            return "";
        }

        #endregion
    }
}
