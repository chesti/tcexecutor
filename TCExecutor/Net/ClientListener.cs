using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

using System.Text.RegularExpressions;

namespace TelentCommandExecutor.Net
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientListener
    {
        #region private/internal
        /// <summary>
        /// Internal delegate which supports client connection closing.
        /// </summary>
        /// <param name="client">Closed clinet connection</param>
        internal delegate void ClientDisconectedHandler(ClientListener client);
        internal event ClientDisconectedHandler clientDisconected;

        /// <summary>
        /// reset event for Listener.
        /// </summary>
        internal ManualResetEvent mThEvent { get; set; }

        private TcpClient mClient;
        private bool mListen;
        private TCExecutor mServer;
        private int mID;

        private StreamReader mReader;
        private StreamWriter mWriter;

        private static Regex etxRemove = new Regex(".*\x03", RegexOptions.Compiled);
        #endregion

        #region private/internal methods
        /// <summary>
        /// Client listener constructor
        /// </summary>
        /// <param name="incomingClient">TCPClient passed from Listener</param>
        /// <param name="server">Main server object.</param>
        /// <param name="id">Client number.</param>
        internal ClientListener(TcpClient incomingClient, TCExecutor server, int id)
        {
            mClient = incomingClient;
            mServer = server;

            mID = id;

            mReader = new StreamReader(mClient.GetStream());
            mWriter = new StreamWriter(mClient.GetStream());

            mWriter.WriteLine(mServer.WelcomeString);
            mWriter.Write(">");
            mWriter.Flush();
        }

        /// <summary>
        /// Listen to user communication (both ways)
        /// </summary>
        /// <param name="state">ManualResetEvent passed to this thread.</param>
        internal void Listen(object state)
        {
            //set the state event
            if (state is ManualResetEvent)
            {
                mThEvent = (ManualResetEvent)state;
            }
            else
            {
                throw new ArgumentException("ManualResetEvent expected");
            }

            mListen = true;
            try
            {
                while (mListen)
                {
                    string line = mReader.ReadLine();
                    if (line != null)
                    {
                        line = etxRemove.Replace(line, string.Empty);
                        if (line != "")
                        {
                            string result = mServer.ExecuteCommand(line);
                            switch (result)
                            {
                                case "exit":
                                    mWriter.WriteLine("Bye!!!");
                                    mWriter.Flush();
                                    mClient.Close();
                                    OnCLinetDisconected(this);
                                    mListen = false;
                                    continue;
                                default:
                                    mWriter.WriteLine(result);
                                    break;
                            }
                        }

                        mWriter.Write(">");
                        mWriter.Flush();
                    }
                    else
                    {
                        OnCLinetDisconected(this);
                        mListen = false;
                    }
                }
            }
            catch (IOException)
            { }
            catch (SocketException)
            { }
            finally
            {
                mThEvent.Set();
            }
        }

        /// <summary>
        /// Method invoked when server is stopping and there are still some connections. 
        /// </summary>
        internal void Stop()
        {
            mListen = false;
            mWriter.Write("\r\nServer stopped.");
            mWriter.Flush();
            mClient.Close();
        }

        /// <summary>
        /// Supports situation when client closes the connection
        /// </summary>
        /// <param name="client">Client whcih closed the connection.</param>
        internal void OnCLinetDisconected(ClientListener client)
        {
            ClientDisconectedHandler handler = clientDisconected;
            if (handler != null)
                handler(client);
        }
        #endregion
    }
}
