using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

using System.Threading;

namespace TelentCommandExecutor.Net
{
    internal class Listener : TcpListener
    {
        #region private/internal
        private TCExecutor mServer;
        private object mLockObject;

        private List<WaitHandle> mWaitHandles;
        private List<ClientListener> mClientListeners;
        #endregion

        #region private/internal methods

        /// <summary>
        /// Listener constructor. Starts listenning thread when TCExecutor is started.
        /// </summary>
        /// <param name="address">Listenning address. Passed through TCExecutor</param>
        /// <param name="server">Master server object.</param>
        internal Listener(IPEndPoint address, TCExecutor server)
            : base(address)
        {
            mServer = server;
            mLockObject = new object();
            mWaitHandles = new List<WaitHandle>();
            mClientListeners = new List<ClientListener>();
        }

        /// <summary>
        /// Start the listener.
        /// </summary>
        internal void Run()
        {
            Start();

            BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), this);
        }

        /// <summary>
        /// Stop the listener.
        /// </summary>
        internal void StopListener()
        {
            if (mWaitHandles.Count > 0)
            {
                foreach (ClientListener c in mClientListeners)
                {
                    c.Stop();
                }

                WaitHandle.WaitAll(mWaitHandles.ToArray());

                mWaitHandles.Clear();
                mClientListeners.Clear();
            }

        }

        /// <summary>
        /// Accept client connection. Asynhronous method.
        /// </summary>
        /// <param name="ar">IAsyncResult</param>
        internal void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            lock (mLockObject)
            {
                TcpClient acceptedClient = EndAcceptTcpClient(ar);

                ManualResetEvent thEvent = new ManualResetEvent(false);
                ClientListener client = new ClientListener(acceptedClient, mServer, mClientListeners.Count);

                mWaitHandles.Add(thEvent);
                mClientListeners.Add(client);

                client.clientDisconected += new ClientListener.ClientDisconectedHandler(OnClientDisconected);

                ThreadPool.QueueUserWorkItem(new WaitCallback(client.Listen), thEvent);
            }
            //wait for another connection
            BeginAcceptTcpClient(new AsyncCallback(DoAcceptTcpClientCallback), this);
        }

        /// <summary>
        /// Supports client disconnect event.
        /// </summary>
        /// <param name="client"></param>
        internal void OnClientDisconected(ClientListener client)
        {
            lock (mLockObject)
            {
                mWaitHandles.Remove(client.mThEvent);
                mClientListeners.Remove(client);
                client = null;
            }
        }
        #endregion

    }
}
